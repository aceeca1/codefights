// Level 1

def add(param1: Int, param2: Int): Int = {
    param1 + param2
}

def centuryFromYear(year: Int): Int = {
    (year + 99) / 100
}

def checkPalindrome(inputString: String): Boolean = {
    inputString == inputString.reverse
}

// Level 2

def adjacentElementsProduct(a: Array[Int]): Int = {
    (0 to a.size - 2).map{i => a(i) * a(i + 1)}.max
}

def shapeArea(n: Int): Int = {
    n * n + (n - 1) * (n - 1)
}

def makeArrayConsecutive2(a: Array[Int]): Int = {
    a.max - a.min + 1 - a.size
}

def almostIncreasingSequence(a: Array[Int]): Boolean = {
    def tryRemove(k: Int) =
        (k == 0 || k == a.size - 1 || a(k - 1) < a(k + 1)) &&
        (k + 1 to a.size - 2).forall{i => a(i) < a(i + 1)}
    for (i <- 0 to a.size - 2)
        if (a(i) >= a(i + 1))
            return tryRemove(i) || tryRemove(i + 1)
    return true
}

def matrixElementsSum(a: Array[Array[Int]]): Int = {
    (0 until a(0).size).map{i =>
        a.map{_(i)}.takeWhile{_ != 0}.sum
    }.sum
}

// Level 3

def allLongestStrings(a: Array[String]): Array[String] = {
    val m = a.map{_.size}.max
    a.filter{_.size == m}
}

def commonCharacterCount(s1: String, s2: String): Int = {
    s1.intersect(s2).size
}

def isLucky(n: Int): Boolean = {
    val s = n.toString
    val mid = s.size >> 1
    val sum1 = (0 until mid).map{i => s(i) - '0'}.sum
    val sum2 = (mid until s.size).map{i => s(i) - '0'}.sum
    sum1 == sum2
}

def sortByHeight(a: Array[Int]): Array[Int] = {
    val b = a.filter{_ != -1}.sorted
    var k = 0
    for (i <- a.indices) if (a(i) != -1) {
        a(i) = b(k)
        k += 1
    }
    a
}

def reverseParentheses(s: String): String = {
    var k = 0
    def parse: StringBuilder = {
        val sb = new StringBuilder
        while (k < s.size) s(k) match {
            case '(' =>
                k += 1
                sb.append(parse.reverse)
                k += 1
            case ')' => return sb
            case c =>
                sb.append(c)
                k += 1
        }
        sb
    }
    parse.toString
}

// Level 4

def alternatingSums(a: Array[Int]): Array[Int] = {
    val ans = Array(0, 0)
    var d = 0
    for (i <- a) {
        ans(d) += i
        d = 1 - d
    }
    ans
}

def addBorder(a: Array[String]): Array[String] = {
    Array.concat(
        Array(new String(Array.fill(a(0).size + 2){'*'})),
        a.map{'*' + _ + '*'},
        Array(new String(Array.fill(a(0).size + 2){'*'}))
    )
}

def areSimilar(a: Array[Int], b: Array[Int]): Boolean = {
    val c = a.indices.filter{i => a(i) != b(i)}
    if (c.size == 0) return true
    if (c.size != 2) return false
    a(c(0)) == b(c(1)) && a(c(1)) == b(c(0))
}

def arrayChange(a: Array[Int]): Int = {
    var ans = 0
    for (i <- 1 until a.size) {
        val target = a(i) max a(i - 1) + 1
        ans += target - a(i)
        a(i) = target
    }
    ans
}

def palindromeRearranging(a: String): Boolean = {
    val b = Array.ofDim[Int](256)
    for (i <- a) b(i) += 1
    b.count{bi => (bi & 1) != 0} <= 1
}

// Level 5

def areEquallyStrong(aL: Int, aR: Int, bL: Int, bR: Int): Boolean = {
    aL == bL && aR == bR ||
    aL == bR && aR == bL
}

def arrayMaximalAdjacentDifference(a: Array[Int]): Int = {
    (0 to a.size - 2).map{i => Math.abs(a(i) - a(i + 1))}.max
}

val reIPv4 = "(\\d+)\\.(\\d+)\\.(\\d+)\\.(\\d+)".r
def isIPv4Address(a: String): Boolean = {
    a match {
        case reIPv4(a1, a2, a3, a4) =>
            a1.toInt < 256 && a2.toInt < 256 &&
            a3.toInt < 256 && a4.toInt < 256
        case _ => false
    }
}

def avoidObstacles(a: Array[Int]): Int = {
    val a1 = a.sorted
    var ans = 1
    def clear(k: Int): Boolean = {
        var ki = k
        var ai = 0
        while (true) {
            if (ai >= a1.size) return true
            if (a1(ai) == ki) return false
            if (a1(ai) < ki) ai += 1
            else ki += k
        }
        false
    }
    while (!clear(ans)) ans += 1
    ans
}

def boxBlur(a: Array[Array[Int]]): Array[Array[Int]] = {
    Array.tabulate(a.size - 2, a(0).size - 2){(i, j) =>
        (
            (a(i    )(j) + a(i    )(j + 1) + a(i    )(j + 2)) +
            (a(i + 1)(j) + a(i + 1)(j + 1) + a(i + 1)(j + 2)) +
            (a(i + 2)(j) + a(i + 2)(j + 1) + a(i + 2)(j + 2))
        ) / 9
    }
}

val mswdX = Array(0, -1, -1, -1,  0,  1, 1, 1)
val mswdY = Array(1,  1,  0, -1, -1, -1, 0, 1)
def minesweeper(a: Array[Array[Boolean]]): Array[Array[Int]] = {
    Array.tabulate(a.size, a(0).size){(i, j) =>
        (0 to 7).map{k =>
            val x = i + mswdX(k)
            val y = j + mswdY(k)
            def xValid = 0 <= x && x < a.size
            def yValid = 0 <= y && y < a(0).size
            if (xValid && yValid && a(x)(y)) 1 else 0
        }.sum
    }
}

// Level 6

def arrayReplace(a: Array[Int], b1: Int, b2: Int): Array[Int] = {
    a.map{ai => if (ai == b1) b2 else ai}
}

def evenDigitsOnly(n: Int): Boolean = {
    n.toString.forall{i => (i & 1) == 0}
}

val reVar = "[A-Za-z_][A-Za-z0-9_]*".r
def variableName(name: String): Boolean = {
    name match {
        case reVar() => true
        case _ => false
    }
}

def alphabeticShift(a: String): String = {
    a.map{ai => ((ai - 'a' + 1) % 26 + 'a').toChar}
}

def chessBoardCellColor(cell1: String, cell2: String): Boolean = {
    ((cell1.sum + cell2.sum) & 1) == 0
}

// Level 7

def circleOfNumbers(n: Int, a: Int): Int = {
    (a + (n >> 1)) % n
}

def depositProfit(deposit: Int, rate: Int, threshold: Int): Int = {
    var ans = 0
    var current = deposit.toDouble
    while (current < threshold) {
        ans += 1
        current *= 1.0 + rate / 100.0
    }
    ans
}

def absoluteValuesSumMinimization(a: Array[Int]): Int = {
    val aS = a.sorted
    aS((a.size - 1) >> 1)
}

def stringsRearrangement(a: Array[String]): Boolean = {
    val e = Array.tabulate(a.size, a.size){(i1, i2) =>
        a(i1).indices.count{k => a(i1)(k) != a(i2)(k)} == 1
    }
    val b = Array.range(0, a.size)
    def put(m: Int): Boolean = {
        if (m == b.size) return true
        for (i <- m until b.size) {
            val bi = b(i)
            if (m == 0 || e(bi)(b(m - 1))) {
                b(i) = b(m)
                b(m) = bi
                if (put(m + 1)) return true
                b(m) = b(i)
                b(i) = bi
            }
        }
        return false
    }
    put(0)
}

// Level 8

def extractEachKth(a: Array[Int], k: Int): Array[Int] = {
    import collection.mutable.Buffer
    var d = 0
    val ans = Buffer[Int]()
    for (i <- a) {
        d += 1
        if (d == k) d = 0
        else ans.append(i)
    }
    ans.toArray
}

def firstDigit(a: String): Char = {
    a.find{_.isDigit}.get
}

def differentSymbolsNaive(s: String): Int = {
    s.distinct.size
}

def arrayMaxConsecutiveSum(a: Array[Int], k: Int): Int = {
    var ans = 0
    var s = 0
    for (i <- 0 until k) s += a(i)
    for (i <- k to a.size) {
        if (s > ans) ans = s
        if (i < a.size) s = s + a(i) - a(i - k)
    }
    ans
}

// Level 9

def growingPlant(upSpeed: Int, downSpeed: Int, desiredHeight: Int): Int = {
    def divUp(a: Int, b: Int) = (a + b - 1) / b
    (divUp(desiredHeight - upSpeed, upSpeed - downSpeed) max 0) + 1
}

def knapsackLight(v1: Int, w1: Int, v2: Int, w2: Int, maxW: Int): Int = {
    var maxV = 0
    if (w1 <= maxW && v1 > maxV) maxV = v1
    if (w2 <= maxW && v2 > maxV) maxV = v2
    val w3 = w1 + w2
    val v3 = v1 + v2
    if (w3 <= maxW && v3 > maxV) maxV = v3
    maxV
}

def longestDigitsPrefix(a: String): String = {
    a.takeWhile{_.isDigit}
}

def digitDegree(n: Int): Int = {
    if (n < 10) 0 else digitDegree(n.toString.map{_ - '0'}.sum) + 1
}

def bishopAndPawn(bishop: String, pawn: String): Boolean = {
    val dx = bishop(0) - pawn(0)
    val dy = bishop(1) - pawn(1)
    dx == dy || dx == -dy
}

// Level 10

def isBeautifulString(a: String): Boolean = {
    val c = Array.ofDim[Int](26)
    for (i <- a) c(i - 'a') += 1
    (0 to 24).forall{i => c(i) >= c(i + 1)}
}

def findEmailDomain(a: String): String = {
    val at = a.lastIndexOf('@')
    a.substring(at + 1)
}

def buildPalindrome(st: String): String = {
    val k = (0 until st.size).filter{i =>
        val a = st.substring(i)
        a == a.reverse
    }.min
    st + st.substring(0, k).reverse
}

def electionsWinners(votes: Array[Int], k: Int): Int = {
    val m = votes.max
    if (k != 0) votes.count{_ + k > m}
    else if (votes.count{_ == m} == 1) 1 else 0
}

val reMac = "([0-9A-F]{2}\\-){5}[0-9A-F]{2}".r
def isMAC48Address(a: String): Boolean = {
    a match {
        case reMac() => true
        case _ => false
    }
}

// Level 11

def isDigit(a: Char): Boolean = {
    a.isDigit
}

def lineEncoding(s: String): String = {
    val sb = new StringBuilder
    var current = '\0'
    var amount = 0
    def currentFinish = amount match {
        case 0 => ()
        case 1 => sb.append(current)
        case n => sb.append(n); sb.append(current)
    }
    for (i <- s)
        if (i == current) amount += 1
        else { currentFinish; current = i; amount = 1 }
    currentFinish
    sb.toString
}

val knightX = Array(-1, -2, -2, -1,  1,  2, 2, 1)
val knightY = Array( 2,  1, -1, -2, -2, -1, 1, 2)
def chessKnight(cell: String): Int = {
    val y = cell(1) - '1'
    (0 to 7).count{i =>
        val x = cell(0) - 'a' + knightX(i)
        val y = cell(1) - '1' + knightY(i)
        def xValid = 0 <= x && x <= 7
        def yValid = 0 <= y && y <= 7
        xValid && yValid
    }
}

def deleteDigit(n: Int): Int = {
    val s = n.toString
    var ans = 0
    for (i <- s.indices) {
        val sb = new StringBuilder(s)
        sb.deleteCharAt(i)
        val t = sb.toInt
        if (t > ans) ans = t
    }
    ans
}

// Level 12

val reWord = "[A-Za-z]+".r
def longestWord(a: String): String = {
    reWord.findAllIn(a).maxBy{_.size}
}

val reTime = "(\\d{2}):(\\d{2})".r
def validTime(time: String): Boolean = {
    time match {
        case reTime(h, m) => h.toInt < 24 && m.toInt < 60
        case _ => false
    }
}

val reNum = "\\d+".r
def sumUpNumbers(a: String): Int = {
    reNum.findAllIn(a).map{_.toInt}.sum
}

def differentSquares(a: Array[Array[Int]]): Int = {
    val b = for {
        i <- 0 to a.size - 2
        j <- 0 to a(0).size - 2
    } yield (a(i)(j), a(i)(j + 1), a(i + 1)(j), a(i + 1)(j + 1))
    b.distinct.size
}

def digitsProduct(a: Int): Int = {
    if (a == 0) return 10
    if (a == 1) return 1
    val sb = new StringBuilder
    var b = a
    for (i <- Range(9, 1, -1)) {
        while (b % i == 0) {
            b /= i
            sb.append(i)
        }
    }
    if (b == 1) sb.reverse.toInt
    else -1
}

def fileNaming(a: Array[String]): Array[String] = {
    import collection._
    val c = new mutable.HashSet[String]
    a.map{ai =>
        val ans = if (!c.contains(ai)) ai else {
            var k = 1
            def getName = ai + "(" + k + ")"
            var cName: String = getName
            while (c.contains(cName)) {
                k += 1
                cName = getName
            }
            cName
        }
        c.add(ans)
        ans
    }
}

def messageFromBinaryCode(a: String): String = {
    a.grouped(8).map{Integer.parseInt(_, 2).toChar}.mkString
}

val sprdX = Array(0, -1,  0, 1)
val sprdY = Array(1,  0, -1, 0)
def spiralNumbers(n: Int): Array[Array[Int]] = {
    val ans = Array.ofDim[Int](n, n)
    def fill(x: Int, y: Int, d: Int, k: Int) {
        if (k > n * n) return
        println(x, y, d, k)
        ans(x)(y) = k
        val xNew = x + sprdX(d)
        val yNew = y + sprdY(d)
        def xValid = 0 <= xNew && xNew < n
        def yValid = 0 <= yNew && yNew < n
        if (xValid && yValid && ans(xNew)(yNew) == 0)
            fill(xNew, yNew, d, k + 1)
        else {
            val dNew = (d + 3) & 3
            val xNew = x + sprdX(dNew)
            val yNew = y + sprdY(dNew)
            fill(xNew, yNew, dNew, k + 1)
        }
    }
    fill(0, 0, 0, 1)
    ans
}

def sudoku(grid: Array[Array[Int]]): Boolean = {
    val a = for {
        x1u <- 0 to 2; x1v <- 0 to 2
        y1u <- 0 to 2; y1v <- 0 to 2
        x2u <- 0 to 2; x2v <- 0 to 2
        y2u <- 0 to 2; y2v <- 0 to 2
        b1 = x1u == x2u && x1v == x2v
        b2 = y1u == y2u && y1v == y2v
        b3 = x1u == x2u && y1u == y2u
        if (b1 || b2 || b3)
        x1 = x1u * 3 + x1v
        y1 = y1u * 3 + y1v
        x2 = x2u * 3 + x2v
        y2 = y2u * 3 + y2v
        if (x1 != x2 || y1 != y2)
    } yield grid(x1)(y1) != grid(x2)(y2)
    a.forall(identity)
}
