#!/usr/bin/fsharpi
open System.Linq

// Intro Gates
let addTwoDigits n = n / 10 + n % 10
let largestNumber n =
    let rec f(n, ans) =
        if n = 0 then ans else f(n - 1, ans * 10 + 9) in f(n, 0)
let candies n m = m / n * n
let seatsInTheater nCols nRows col row =
    (nCols - col + 1) * (nRows - row)
let maxMultiple divisor bound = bound / divisor * divisor
let circleOfNumbers n firstNumber = (firstNumber + n / 2) % n
let lateRide n =
    let dsum(k) = k / 10 + k % 10 in dsum(n / 60) + dsum(n % 60)
let phoneCall min1 min2_10 min11 S =
    if S < min1 then 0 else
        let S1 = S - min1
        if S1 < min2_10 * 9 then 1 + S1 / min2_10 else
            10 + (S1 - min2_10 * 9) / min11

// At the Crossroads
let reachNextLevel experience threshold reward =
    experience + reward >= threshold
let rec knapsackLight value1 weight1 value2 weight2 maxW =
    if value1 > value2 then
        knapsackLight value2 weight2 value1 weight1 maxW
    elif maxW >= weight1 + weight2 then value1 + value2
    elif maxW >= weight2 then value2
    elif maxW >= weight1 then value1
    else 0
let extraNumber a b c = a ^^^ b ^^^ c
let isInfiniteProcess a b = not (b >= a && (b - a) &&& 1 = 0)
let arithmeticExpression A B C =
    A + B = C || A - B = C || A * B = C || A = B * C
let rec tennisSet score1 score2 =
    if score1 = score2 then false
    elif score1 > score2 then tennisSet score2 score1 else
        score2 = 7 && score1 >= 5 ||
        score2 = 6 && score1 < 5
let willYou young beautiful loved = loved <> (young && beautiful)
let metroCard lastNumberOfDays =
    match lastNumberOfDays with
    | 28 -> [| 31; |]
    | 30 -> [| 31; |]
    | 31 -> [| 28; 30; 31; |]
    | _ -> failwith "bad input"

// Corner of 0s and 1s
let killKthBit n k = n &&& ~~~(1 <<< (k - 1)) //>>
let arrayPacking(a: int[]) =
    a.Reverse().Aggregate(fun s i -> (s <<< 8) + i) //>>
let rangeBitCount a b =
    let rec bc(n, ans) =
        match n with 0 -> ans | _ -> bc(n >>> 1, ans + (n &&& 1))
    let rec f(n, c, k, ans) =
        if n = 0 then ans
        elif n &&& 1 <> 0 then
            let ans1 = ans + (k <<< (k - 1)) + ((c - 1) <<< k) //>>>>
            f(n >>> 1, c - 1, k + 1, ans1)
        else f(n >>> 1, c, k + 1, ans)
    let ff(n) = f(n, bc(n, 0), 0, 0) in ff(b + 1) - ff(a)
let mirrorBits a =
    let rec f(n, ans) =
        if n = 0 then ans else
            f(n >>> 1, ans + ans + (n &&& 1)) in f(a, 0)
let secondRightmostZeroBit n =
    let n1 = ~~~n
    let n2 = n1 &&& (n1 - 1)
    n2 &&& (-n2)
let swapAdjacentBits n =
    ((n >>> 1) &&& 0x55555555) + ((n &&& 0x55555555) <<< 1) //>>
let differentRightmostBit n m = let k = n ^^^ m in k &&& -k
let equalPairOfBits n m = let k = ~~~(n ^^^ m) in k &&& -k

// Loop Tunnel
let leastFactorial n =
    let rec f(m, k) =
        if m >= n then m else f(m * (k + 1), k + 1) in f(1, 1)
let countSumOfTwoRepresentations2 n l r =
    (max(0)(min(n - l)(r) - max(n - r)(l) + 1) + 1) >>> 1
let magicalWell(a: int)(b: int) n =
    (seq { for i = 0 to n - 1 do yield (a + i) * (b + i) }).Sum()
let lineUp(commands: string) =
    let turn(c: char) =
        match c with
        | 'R' -> 1 | 'A' -> 2 | 'L' -> 3
        | _ -> failwith "bad input"
    commands.Aggregate((0, 0), fun (d, ans) c ->
        let d1 = (d + turn c) &&& 3 in d1,
        if d1 &&& 1 = 0 then ans + 1 else ans) |> snd
let additionWithoutCarrying param1 param2 =
    let rec f(n1, n2, k, ans) =
        if    n1 = 0 then ans + n2 * k
        elif  n2 = 0 then ans + n1 * k
        else  f(n1 / 10, n2 / 10, k * 10, ans + (n1 + n2) % 10 * k)
    f(param1, param2, 1, 0)
let appleBoxes k =
    let f(n) = n * (1 + n) * (1 + n + n) / 6
    let red = f(k / 2) <<< 2 //>>
    red - (f(k) - red)
let rec increaseNumberRoundness n =
    let rec f(n) = n > 0 && (n % 10 = 0 || f(n / 10))
    if n % 10 <> 0 then f(n) else
        increaseNumberRoundness(n / 10)
let rounders value =
    let rec f(n, ans) =
        if n < 10 then n * ans
        elif n % 10 >= 5 then f(n / 10 + 1, ans * 10)
        else f(n / 10, ans * 10) in f(value, 1)
let candles c makeNew =
    let rec f(n, ans) =
        if n < makeNew then ans else
            let cc = n / makeNew
            f(n - cc * makeNew + cc, ans + cc) in f(c, c)
let countBlackCells n m =
    let n64 = int64 n
    let m64 = int64 m
    let f(k) =
        let k64 = int64 k
        let lb = (k64 * m64 + n64 - 1L) / n64 - 1L |> int
        let ub = (k64 * m64 + m64) / n64 |> int in ub - lb + 1
    (seq { for i = 0 to n - 1 do yield f(i) }).Sum() - 2

// List Forest Edge
let createArray size = Array.create(size)(1)
let arrayReplace(arr: int[]) elemToReplace substitutionElem =
    arr |> Array.map(fun i ->
        if i = elemToReplace then substitutionElem else i)
let firstReverseTry(arr: int[]) =
    Array.init(arr.Length)(fun i ->
        if i = 0 then arr.[arr.Length - 1]
        elif i = arr.Length - 1 then arr.[0]
        else arr.[i])
let concatenateArrays a b = [| a; b; |] |> Array.concat
let removeArrayPart(arr: int[]) l r =
    Array.init(arr.Length - (r - l + 1))(fun i ->
        if i < l then arr.[i] else arr.[i + (r - l + 1)])
let isSmooth(arr: int[]) =
    arr.[0] = arr.[arr.Length - 1] &&
        let m = arr.Length >>> 1
        let o = arr.Length &&& 1 <> 0
        arr.[0] = if o then arr.[m] else arr.[m] + arr.[m - 1]
let replaceMiddle(arr: int[]) =
    if arr.Length &&& 1 <> 0 then arr else
        let m = arr.Length >>> 1
        Array.init(arr.Length - 1)(fun i ->
            if i < m - 1 then arr.[i]
            elif i = m - 1 then arr.[i] + arr.[i + 1]
            else arr.[i + 1])
let makeArrayConsecutive2(arr: int[]) =
    arr.Max() - arr.Min() + 1 - arr.Length

// Book Market
let encloseInBrackets inputString =
    System.String.Concat('(', inputString, ')')
let properNounCorrection(noun: string) =
    let a0 = System.Char.ToUpper noun.[0]
    let an = noun.[1 ..].ToLower()
    System.String.Concat(a0, an)
let isTandemRepeat(s: string) =
    let len = s.Length / 2 in s.[.. len - 1] = s.[len ..]
let isCaseInsensitivePalindrome(s: string) =
    let ss = s.ToLower() in ss.Reverse().SequenceEqual(ss)
let findEmailDomain(s: string) = s.[s.LastIndexOf('@') + 1 ..]
let htmlEndTagByStartTag(startTag: string) =
    let c1 = startTag.IndexOf(' ')
    let c2 = if c1 = -1 then startTag.Length - 1 else c1
    System.String.Concat("</", startTag.[1 .. c2 - 1], '>')
let isMAC48Address(s: string) =
    s.Length = 17 &&
    let hex c = ('0' <= c && c <= '9') || ('A' <= c && c <= 'F')
    (seq { 0 .. 3 .. 15 }).All(fun i ->
        (hex s.[i]) && (hex s.[i + 1]) &&
        (i + 2 = s.Length || s.[i + 2] = '-'))
let isUnstablePair(s1: string)(s2: string) =
    s1 < s2 && s2.ToLower() < s1.ToLower() ||
    s2 < s1 && s1.ToLower() < s2.ToLower()

// Labyrinth of Nested Loops
let isPower n =
    let rec f(k) =
        let a = round(float n ** (1.0 / float k)) |> int
        a > 1 && (pown a k = n || f(k + 1))
    n = 1 || f(2)
let isSumOfConsecutive2 n =
    let rec f(m) = match m &&& 1 with 0 -> f(m >>> 1) | _ -> m
    let g(m) = (seq { 2 .. m }).Count(fun i -> m % i = 0)
    f(n) |> g
let squareDigitsSequence a0 =
    let findCycle(f, a) =
        let rec g(n1, a1, n2, a2) =
            if a1 = a2 then n2 - n1
            elif n2 > n1 + n1 then g(n2, a2, n2 + 1, f(a2))
            else g(n1, a1, n2 + 1, f(a2)) in g(0, a, 1, f(a))
    let findStart(f, a, c) =
        let rec g(n, a1, a2) =
            if a1 = a2 then n else g(n + 1, f(a1), f(a2))
        let rec h(n, a) = if n = 0 then a else h(n - 1, f(a))
        g(c, a, h(c, a))
    let rec f(ans)(a) =
        if a = 0 then ans else
            let a0 = a % 10 in f(ans + a0 * a0)(a / 10)
    findStart(f(0), a0, findCycle(f(0), a0)) + 1
let rec pagesNumberingWithInk current numberOfDigits =
    let rec f(n, ans) =
        match n with 0 -> ans | _ -> f(n / 10, ans + 1)
    let c0 = f(current, 0)
    if numberOfDigits < c0 then current - 1 else
        pagesNumberingWithInk(current + 1)(numberOfDigits - c0)
let comfortableNumbers L R =
    let delta = L - 1
    let rec ilogb(n, ans) =
        match n with 0 -> ans | _ -> ilogb(n >>> 1, ans + 1)
    let m = 1 <<< (ilogb(R - delta + 2, -1) + 1) //>>
    let a = Array.create(m + m)(1)
    for i = m - 1 downto 1 do
        a.[i] <- a.[i + i] + a.[i + i + 1]
    let aQ(n) =
        let rec f(k, ans) =
            if k = 1 then ans
            elif k &&& 1 <> 0 then f(k >>> 1, ans + a.[k - 1])
            else f(k >>> 1, ans) in f(n + m, 0)
    let aR(n) =
        let rec f(k) =
            if k > 0 then a.[k] <- a.[k] - 1; f(k >>> 1)
        f(n + m)
    let rec s(n, ans) =
        if n <= 0 then ans else s(n / 10, ans + n % 10)
    let sa = Array.init(R - delta + 2)(fun i -> s(i + delta, 0))
    let comfortable(k) =
        aR(k)
        let lb = max(k - sa.[k] - 1)(0)
        let ub = min(k + sa.[k] + 1)(R - delta + 1)
        aQ(ub) - aQ(lb + 1)
    let ord = [| 1 .. R - delta |].OrderBy(fun i -> sa.[i])
    ord.Select(comfortable).Sum()
let weakNumbers m =
    let p = Array.zeroCreate<int>(m + 1)
    let a = Array.zeroCreate<int>(m + 1)
    let b = Array.zeroCreate<int>(m + 1)
    a.[1] <- 1; b.[1] <- 1
    let mutable u = 0
    for i = 2 to m do
        let mutable v = p.[i]
        if v = 0 then v <- i; p.[u] <- i; u <- i
        let mutable w = 2
        let mutable brk = false
        while not brk && i * w <= m do
            p.[i * w] <- w
            if w < v then w <- p.[w] else brk <- true
        let v2 = min(i / v)(p.[i / v])
        if v = v2 then
            a.[i] <- a.[i / v]
            b.[i] <- b.[i / v] + 1
        else
            a.[i] <- a.[i / v] * b.[i / v]
            b.[i] <- 2
    for i = 0 to m do a.[i] <- a.[i] * b.[i]
    let c = Array.zeroCreate<int>(a.Max() + 1)
    let s = seq {
        for i = 1 to m do
            yield c.[a.[i]]
            for j = 1 to a.[i] - 1 do c.[j] <- c.[j] + 1 }
    let w, t = s.Aggregate((0, 0), fun (w, t) i ->
        if i > w then (i, 1) elif i = w then (w, t + 1) else (w, t))
    [| w; t; |]
let rectangleRotation a b =
    let f(n, k) =
        let m = (sqrt 2.0 * float n * 0.5 + float k) * 0.5 |> int
        m + m + 1 - k in f(a, 0) * f(b, 0) + f(a, 1) * f(b, 1)
let crosswordFormation(w: string[]) =
    let c = Array.init(3)(fun i ->
        [| for j = 0 to w.[3].Length - 1 do
            for k = 0 to w.[i].Length - 1 do
                if w.[3].[j] = w.[i].[k] then yield j, k |])
    let ans = ref 0
    let accu(a12, i1, b1, i2, b2, i) =
        let i3 = 3 - i1 - i2
        for k = a12 to w.[i3].Length - 1 do
            let e13() = w.[i1].[b1 + i] = w.[i3].[k - a12]
            let e23() = w.[i2].[b2 + i] = w.[i3].[k]
            if e13() && e23() then ans := !ans + 1
    for i1 = 0 to 2 do
        for i2 = 0 to 2 do
            if i2 <> i1 then
                for a1, b1 in c.[i1] do
                    for a2, b2 in c.[i2] do
                        let a12 = a2 - a1
                        if a12 >= 2 then
                            let lb = min(b1)(b2)
                            let c1 = w.[i1].Length - 1 - b1
                            let c2 = w.[i2].Length - 1 - b2
                            let ub = min(c1)(c2)
                            for i = -lb  to -2 do
                                accu(a12, i1, b1, i2, b2, i)
                            for i = 2    to ub do
                                accu(a12, i1, b1, i2, b2, i)
    !ans <<< 1 //>>

// Lab of Transformations
let characterParity symbol =
    if symbol < '0' || '9' < symbol then "not a digit"
    elif int symbol &&& 1 <> 0 then "odd" else "even"
let reflectString s =
    s |> String.map(fun i -> int('a' + 'z') - (int i) |> char)
let newNumeralSystem(c: char) =
    let s = (int c) - (int 'A')
    let ans = ResizeArray<string>()
    let mutable i = 0
    while i <= s - i do
        System.String.Concat(
            char i + 'A', " + ", char(s - i) + 'A') |> ans.Add
        i <- i + 1
    ans.ToArray()
let cipher26(s: string) =
    let a = Array.init(s.Length)(fun i ->
        if i = 0 then s.[0] else
            'a' + char((int s.[i] + 26 - (int s.[i - 1])) % 26))
    System.String(a)
let stolenLunch note =
    note |> String.map(fun i ->
        if    '0' <= i && i <= '9' then
            ((int i) - (int '0') |> char) + 'a'
        elif  'a' <= i && i <= 'j' then
            ((int i) - (int 'a') |> char) + '0'
        else i)
let higherVersion ver1 ver2 =
    let parse(s: string) = s.Split('.').Select(int)
    Seq.compareWith(-)(parse ver1)(parse ver2) > 0
let decipher(s: string) =
    let ans = System.Text.StringBuilder()
    let mutable buf = 0
    for i in s do
        buf <- buf * 10 + (int i) - (int '0')
        if int 'a' <= buf then
            ans.Append(char buf) |> ignore
            buf <- 0
    ans.ToString()
let alphanumericLess s1 s2 =
    let parse(s: string) =
        let ans = ResizeArray<int64>()
        let buf = System.Text.StringBuilder()
        let flush() =
            if buf.Length > 0 then
                ans.Add(buf.ToString() |> int64)
                buf.Clear() |> ignore
        let M = System.Int64.MaxValue - 256L
        for i in s do
            if '0' <= i && i <= '9' then
                ignore(buf.Append(i))
            else
                flush(); ignore(ans.Add(int64 i + M))
        flush(); ans
    let cmp = Seq.compareWith(fun (i1: int64) i2 -> i1.CompareTo i2)
    let ans = cmp(parse s1)(parse s2)
    let rewrite(s: string) = s |> String.map(fun i ->
        if i = '0' then char 255 else i)
    ans < 0 || ans = 0 && rewrite(s1) > rewrite(s2)

// Mirror Lake
let stringsConstruction(s1: string)(s2: string) =
    (s1 |> Seq.countBy(id)).GroupJoin(s2,
        (fun (i, _) -> i), (fun i -> i),
        (fun (_, i1) i2 -> i2.Count() / i1)).Min()
let isSubstitutionCipher string1 string2 =
    let norm(s: string) =
        let a = Array.zeroCreate<int>(256)
        Array.init(s.Length)(fun i ->
            let si = int s.[i]
            if a.[si] = 0 then
                a.[0] <- a.[0] + 1
                a.[si] <- a.[0]
            a.[si])
    norm(string1) = norm(string2)
let createAnagram(s1: string)(s2: string) =
    let f(n) = ((s1.Length + s2.Length) >>> 1) - n
    (s1 |> Seq.countBy(id)).GroupJoin(s2,
        (fun (i, _) -> i), (fun i -> i),
        (fun (_, i1) i2 -> min(i1)(i2.Count()))).Sum() |> f
let constructSquare(str: string) =
    let hash(n) =
        let kMul = 0x9ddfea08eb382d69UL
        let h(n) = let m = n * kMul in m ^^^ (m >>> 47)
        h(h(uint64 n))
    let norm(s: string) =
        s |> Seq.countBy(id) |> Seq.map(fun (_, i) -> hash i)
        |> Seq.reduce(+)
    let str0 = norm str
    let rec pow10(n, ans) =
        match n with 0 -> ans | _ -> pow10(n - 1, ans * 10)
    let lb = pow10(str.Length - 1, 1)
    let ub = lb * 10
    let mutable i = ub |> float |> sqrt |> int
    let f(n) = n >= lb && norm(string n) <> str0
    while f(i * i) do i <- i - 1
    if i * i < lb then -1 else i * i
let numbersGrouping(a: int[]) =
    a.Select(fun i -> (i - 1) / 10000).Distinct().Count()
    + a.Length
let differentSquares(matrix: int[][]) =
    let hash(n) =
        let kMul = 0x9ddfea08eb382d69UL
        let h(n) = let m = n * kMul in m ^^^ (m >>> 47)
        h(h(uint64 n))
    let hash4(n1, n2, n3, n4) =
        let inline (++) k1 k2 = hash(uint64 k1) + (uint64 k2)
        n1 ++ n2 ++ n3 ++ n4
    let c = seq {
        for i = 0 to matrix.Length - 2 do
            for j = 0 to matrix.[0].Length - 2 do
                yield hash4(
                    matrix.[i].[j],     matrix.[i].[j + 1],
                    matrix.[i + 1].[j], matrix.[i + 1].[j + 1]) }
    c.Distinct().Count()
let mostFrequentDigitSum n =
    let rec s(p, ans) =
        match p with 0 -> ans | _ -> s(p / 10, ans + p % 10)
    let se = seq {
        let a = ref n
        while !a <> 0 do
            let sa = s(!a, 0)
            yield sa
            a := !a - sa
        yield 0
    }
    se |> Seq.countBy(id) |> Seq.map(fun (k, v) -> v, k)
    |> Seq.max |> snd
let numberOfClans(divisors: int[])(k) =
    let norm(n) =
        let rec f(u, ans) =
            if u >= divisors.Length then ans
            elif n % divisors.[u] = 0 then f(u + 1, ans + ans + 1)
            else f(u + 1, ans + ans)
        f(0, 0)
    (seq { for i = 1 to k do yield norm(i) }).Distinct().Count()

// Well of Integration
let houseNumbersSum(a: int[]) =
    a.TakeWhile(fun i -> i <> 0).Sum()
let allLongestStrings(s: string[]) =
    let m = s.Select(fun (i: string) -> i.Length).Max()
    s |> Array.filter(fun i -> i.Length = m)
let houseOfCats legs =
    let m = legs >>> 1 in [| m &&& 1 .. 2 .. m |]
let alphabetSubsequence(s: string) =
    (seq { 0 .. s.Length - 2 }).All(fun i -> s.[i] < s.[i + 1])
let minimalNumberOfCoins(coins: int[])(price) =
    let rec f(u, p, ans) =
        if u < 0 then ans else
            f(u - 1, p % coins.[u], ans + p / coins.[u])
    f(coins.Length - 1, price, 0)
let addBorder(s: string[]) =
    Array.init(s.Length + 2)(fun i ->
        if i = 0 || i = s.Length + 1 then
            System.String('*', s.[0].Length + 2)
        else
            System.String.Concat('*', s.[i - 1], '*'))
let switchLights(a: int[]) = Array.scanBack(^^^)(a.[1 .. ])(0)
let timedReading maxLength text =
    let re = System.Text.RegularExpressions.Regex("[A-Za-z]+")
    let matches =
        re.Matches(text).Cast<
            System.Text.RegularExpressions.Match>()
    matches.Count(fun i -> i.Value.Length <= maxLength)
let electionsWinners(votes: int[])(k) =
    let m = votes.Max()
    let v = votes.Count(fun i -> m - i < k)
    if k = 0 && votes.Count(fun i -> i = m) = 1 then v + 1 else v
let integerToStringOfFixedWidth number width =
    let s = string number
    if width <= s.Length then s.[s.Length - width ..] else
        System.String('0', width - s.Length) + s
let areSimilar(a1: int[])(a2: int[]) =
    a1 = a2 ||
    let neq = Seq.find(fun i -> a1.[i] <> a2.[i])
    let i1 = (seq { 0 .. a1.Length - 1 }) |> neq
    let i2 = (seq { a1.Length - 1 .. -1 .. 0 }) |> neq
    let a1i1 = a1.[i1]
    a1.[i1] <- a1.[i2]
    a1.[i2] <- a1i1
    a1 = a2
let adaNumber(line: string) =
    let s1 = line.ToCharArray()
    let s2 = s1 |> Array.filter(fun i -> i <> '_')
    let s  = System.String(s2)
    s.Length > 0 &&
    let is(b, n: string) =
        2 <= b && b <= 16 && n.Length > 0 &&
        let a = Array.zeroCreate<bool>(256)
        let set(from, len) =
            for i = int from to int from + len - 1 do
                a.[i] <- true
        if b <= 10 then set('0', b) else
            set('0', 10); set('A', b - 10); set('a', b - 10)
        n.All(fun i -> a.[int i])
    if s.[s.Length - 1] <> '#' then is(10, s) else
        match s.IndexOf('#') with
        | sh when sh = 0 || sh = s.Length - 1 -> false
        | sh ->
        match System.Int32.TryParse s.[.. sh - 1] with
        | true, n -> is(n, s.[sh + 1 .. s.Length - 2])
        | _ -> false
let threeSplit a =
    let a0 = a |> Array.scan(+)(0)
    let m1 = a0.[a0.Length - 1] / 3
    if a.Length = 0 then 0
    elif m1 = 0 then
        let z = a0.Count(fun i -> i = 0)
        (z - 2) * (z - 3) >>> 1
    else
        let m2 = m1 + m1
        a0.Count(fun i -> i = m1) * a0.Count(fun i -> i = m2)

// List Backwoods
let extractMatrixColumn(a: int[][])(i) =
    a |> Array.map(fun ai -> ai.[i])
let areIsomorphic array1 array2 =
    let f(a: int[][]) = a.Select(fun (i: int[]) -> i.Length)
    (f array1).SequenceEqual(f array2)
let reverseOnDiagonals(a: int[][]) =
    let swap(x1, y1, x2, y2) =
        let ax1y1 = a.[x1].[y1]
        a.[x1].[y1] <- a.[x2].[y2]
        a.[x2].[y2] <- ax1y1
    for i = 0 to (a.Length >>> 1) - 1 do
        let r = a.Length - 1 - i
        swap(i, i, r, r)
        swap(i, r, r, i) done; a
let swapDiagonals(a: int[][]) =
    let swap(x1, y1, x2, y2) =
        let ax1y1 = a.[x1].[y1]
        a.[x1].[y1] <- a.[x2].[y2]
        a.[x2].[y2] <- ax1y1
    for i = 0 to a.Length - 1 do
        swap(i, i, i, a.Length - 1 - i) done; a
let crossingSum(a: int[][])(x)(y) =
    a.Select(fun (i: int[]) -> i.[y]).Sum() +
    a.[x].Sum() - a.[x].[y]
let drawRectangle(a: char[][])(b: int[]) =
    let fill(x, y, c) =
        for i in x do for j in y do a.[i].[j] <- c
    match b with
    | [| y1; x1; y2; x2; |] ->
        fill([| x1 .. x2 |], [| y1; y2;  |], '|')
        fill([| x1; x2;  |], [| y1 .. y2 |], '-')
        fill([| x1; x2;  |], [| y1; y2;  |], '*'); a
    | _ -> failwith "bad input"
let volleyballPositions(a: string[][])(k) =
    let pX = [| 3; 1; 0; 1; 3; 2; |]
    let pY = [| 2; 2; 1; 0; 0; 1; |]
    for i = 1 to k % 6 do
        let a0 = a.[pX.[0]].[pY.[0]]
        for i = 0 to 4 do
            a.[pX.[i]].[pY.[i]] <- a.[pX.[i + 1]].[pY.[i + 1]]
        a.[pX.[5]].[pY.[5]] <- a0 done; a
let starRotation(a: int[][])(w)(c)(t) =
    match c with
    | [| cx; cy; |] ->
        for hw = 1 to w >>> 1 do
            let pX = [|
                cx - hw; cx - hw; cx - hw; cx
                cx + hw; cx + hw; cx + hw; cx |]
            let pY = [|
                cy - hw; cy; cy + hw; cy + hw
                cy + hw; cy; cy - hw; cy - hw |]
            for i = 1 to t &&& 7 do
                let a7 = a.[pX.[7]].[pY.[7]]
                for j = 7 downto 1 do
                    a.[pX.[j]].[pY.[j]] <- a.[pX.[j - 1]].[pY.[j - 1]]
                a.[pX.[0]].[pY.[0]] <- a7
        a
    | _ -> failwith "bad input"

// Spring of Integration
let rec arrayConversion(a: int[]) =
    let reduce(op)(e: int[]) =
        Array.init(e.Length >>> 1)(fun i ->
            op e.[i + i] e.[i + i + 1])
    if a.Length = 1 then a.[0] else
        let a0 = a |> reduce(+)
        if a0.Length = 1 then a0.[0] else
            arrayConversion(a0 |> reduce( *))
let arrayPreviousLess(a: int[]) =
    let s = System.Collections.Generic.Stack<int>()
    a |> Array.map(fun i ->
        while s.Count > 0 && s.Peek() >= i do s.Pop() |> ignore
        let ans = if s.Count > 0 then s.Peek() else -1
        s.Push(i); ans)
let pairOfShoes(shoes: int[][]) =
    shoes.GroupBy(
        (fun (i: int[]) -> i.[1]),
        (fun (i: int[]) -> i.[0]),
        (fun _ (i: seq<int>) -> i.Sum(fun x -> x + x - 1)))
        .All(fun i -> i = 0)
let combs c1 c2 =
    let rec num(c: string, u, ans) =
        if u >= c.Length then ans
        elif c.[u] = '*' then num(c, u + 1, ans + ans + 1)
        else num(c, u + 1, ans + ans)
    let f(n1, n2) =
        let mutable i = 0
        while n1 &&& (n2 >>> i) <> 0 do i <- i + 1 done; i
    let n1 = num(c1, 0, 0)
    let n2 = num(c2, 0, 0)
    let a1 = max(f(n1, n2) + c1.Length)(c2.Length)
    let a2 = max(f(n2, n1) + c2.Length)(c1.Length)
    min a1 a2
let stringsCrossover(a: string[])(s: string) =
    let b = a |> Array.map(fun i ->
        Seq.map2(fun c1 c2 -> if c1 = c2 then 1 else 0)(i)(s)
        |> Seq.reduce(fun s k -> s + s + k))
    let m = (1 <<< s.Length) - 1 //>>
    seq {
        for i = 0 to b.Length - 1 do
            for j = i + 1 to b.Length - 1 do
                yield if b.[i] ||| b.[j] = m then 1 else 0 }
    |> Seq.sum
let cyclicString(s: string) =
    let is(n) = s.[.. s.Length - 1 - n] = s.[n ..]
    let rec f(n) = if is(n) then n else f(n + 1) in f(1)
let beautifulText(s: string)(l)(r) =
    let ss = s.Split().Select(fun (i: string) -> i.Length + 1)
    let su = ss |> Seq.scan(+)(0) |> Seq.toArray
    (seq { l + 1 .. r + 1 }).Any(fun i ->
        su.[su.Length - 1] % i = 0 &&
        su.Count(fun si -> si % i = 0) = su.[su.Length - 1] / i + 1)
let runnersMeetings(a: int[])(b: int[]) =
    seq {
        for i = 0 to a.Length - 1 do
            for j = i + 1 to a.Length - 1 do
                if b.[i] <> b.[j] then
                    let t = (a.[j] - a.[i]) / (b.[i] - b.[j])
                    if t >= 0 then yield t, b.[i] * t + a.[i] }
    |> Seq.countBy(id) |> Seq.map(snd) |> Seq.fold(max)(0)
    |> (fun n -> n + n) |> float |> sqrt |> int |> (+) 1
let christmasTree lev h =
    let w = lev + h
    let f(n) = System.String(Array.init(w + 1 + n)(fun i ->
        if w - n <= i then '*' else ' '))
    [|
        yield f(0); yield f(0); yield f(1);
        for i = 1 to lev do
            for j = 1 to h do yield f(i + j)
        for i = 1 to lev do yield f(h >>> 1) |]
let fileNaming names =
    let s = System.Collections.Generic.HashSet<string>()
    names |> Array.map(fun i ->
        let rec f(k) =
            let ss = System.String.Concat(i, '(', k, ')')
            if s.Contains(ss) then f(k + 1) else s.Add(ss) |> ignore; ss
        if s.Contains(i) then f(1) else s.Add(i) |> ignore; i)

// Waterfall of Integration
let sudoku(a: int[][]) =
    let hash(n) =
        let kMul = 0x9ddfea08eb382d69UL
        let h(n) = let m = n * kMul in m ^^^ (m >>> 47)
        h(h(uint64 n))
    let mutable hh = 0UL
    for i = 1 to 9 do hh <- hh - hash(i)
    let h = hh
    seq { 0 .. 8 } |> Seq.forall(fun i ->
        let mutable h1 = h
        let mutable h2 = h
        for j = 0 to 8 do
            h1 <- h1 + hash(a.[i].[j])
            h2 <- h2 + hash(a.[j].[i])
        h1 = 0UL && h2 = 0UL) &&
    let check(i1, j1) =
        let mutable h3 = h
        for i2 = 0 to 2 do
            for j2 = 0 to 2 do
                h3 <- h3 + hash(a.[i1 * 3 + i2].[j1 * 3 + j2])
        h3 = 0UL
    seq {
        for i1 = 0 to 2 do
            for j1 = 0 to 2 do
                yield check(i1, j1) } |> Seq.forall(id)
let minesweeper(a: bool[][]) =
    Array.init(a.Length)(fun i ->
        Array.init(a.[0].Length)(fun j ->
            seq {
                for i1 = i - 1 to i + 1 do
                    if 0 <= i1 && i1 < a.Length then
                        for j1 = j - 1 to j + 1 do
                            if 0 <= j1 && j1 < a.[0].Length then
                                yield if a.[i1].[j1] then 1 else 0
                yield if a.[i].[j] then -1 else 0 } |> Seq.sum))
let boxBlur(a: int[][]) =
    Array.init(a.Length - 2)(fun i ->
        Array.init(a.[0].Length - 2)(fun j ->
            seq {
                for i1 = i to i + 2 do
                    for j1 = j to j + 2 do
                        yield a.[i1].[j1] }
            |> Seq.sum |> (fun n -> n / 9)))
let contoursShifting(a: int[][]) =
    let rec cycle(pX, pY, rev) =
        if rev then
            cycle(
                pX |> Array.rev,
                pY |> Array.rev, false)
        else
            let a0 = a.[pX.[pX.Length - 1]].[pY.[pY.Length - 1]]
            for i = pX.Length - 1 downto 1 do
                a.[pX.[i]].[pY.[i]] <- a.[pX.[i - 1]].[pY.[i - 1]]
            a.[pX.[0]].[pY.[0]] <- a0
    let rec f(x1, x2, y1, y2, rev) =
        if x1 > x2 || y1 > y2 then ()
        elif x1 = x2 then
            let pX = Array.create(y2 - y1 + 1)(x1)
            let pY = [| y1 .. y2 |]
            cycle(pX, pY, rev)
        elif y1 = y2 then
            let pX = [| x1 .. x2 |]
            let pY = Array.create(x2 - x1 + 1)(y1)
            cycle(pX, pY, rev)
        else
            let p = [|
                for i = y1 to y2 - 1 do yield x1, i
                for i = x1 to x2 - 1 do yield i, y2
                for i = y2 downto y1 + 1 do yield x2, i
                for i = x2 downto x1 + 1 do yield i, y1 |]
            let pX = p |> Array.map(fst)
            let pY = p |> Array.map(snd)
            cycle(pX, pY, rev)
            f(x1 + 1, x2 - 1, y1 + 1, y2 - 1, not rev)
    f(0, a.Length - 1, 0, a.[0].Length - 1, false); a
let polygonPerimeter(a: bool[][]) =
    let aT = Array.init(a.[0].Length)(fun i ->
        a |> Array.map(fun ai -> ai.[i]))
    let mutable ans = 0
    for a in [| a; aT |] do
        for ai in a do
            for j = 0 to ai.Length do
                let a1 = j > 0 && ai.[j - 1]
                let a2 = j < ai.Length && ai.[j]
                if a1 <> a2 then ans <- ans + 1
    ans
let gravitation(a: string[]) =
    let c = Array.init(a.[0].Length)(fun i ->
        let se = a.Select(fun (ai: string) -> ai.[i])
        match se |> Seq.tryFindIndex(fun i -> i = '#') with
        | Some n -> a.Length - n - se.Count(fun i -> i = '#')
        | _ -> 0)
    let m = c.Min()
    [| for i = 0 to c.Length - 1 do if c.[i] = m then yield i |]
let isInformationConsistent(ev: int[][]) =
    seq {
        for i = 0 to ev.[0].Length - 1 do
            let evi = ev.Select(fun (k: int[]) -> k.[i])
            yield evi.Any(fun k -> k = 1) && evi.Any(fun k -> k = -1)
    } |> Seq.exists(id) |> not
let correctNonogram(n)(a: string[][]) =
    let aT = Array.init(a.[0].Length)(fun i ->
        a |> Array.map(fun ai -> ai.[i]))
    let k = (n + 1) >>> 1
    let check(c: string[]) =
        let s1 = seq {
            let sh = ref 0
            for i = k to k + n - 1 do
                if c.[i] = "#" then sh := !sh + 1
                elif !sh > 0 then yield !sh; sh := 0
            if !sh > 0 then yield !sh }
        let s2 = seq {
            for i = 0 to k - 1 do
                if c.[i] <> "-" then yield int c.[i] }
        s1.SequenceEqual(s2)
    seq {
        for a in [| a; aT |] do
            for i = k to k + n - 1 do
                yield check a.[i] } |> Seq.forall(id)

// Sorting Outpost
let shuffledArray(a: int[]) =
    let k = System.Array.IndexOf(a, a.Sum() / 2)
    let b = Array.init(a.Length - 1)(fun i ->
        if i < k then a.[i] else a.[i + 1])
    System.Array.Sort b; b
let sortByHeight(a: int[]) =
    let b = a |> Array.filter(fun i -> i <> -1)
    System.Array.Sort b
    let mutable k = 0
    for i = 0 to a.Length - 1 do
        if a.[i] <> -1 then a.[i] <- b.[k]; k <- k + 1 done; a
let sortByLength(a: string[]) =
    a.OrderBy(fun i -> i.Length).ToArray()
let boxesPacking(len: int[])(w: int[])(h: int[]) =
    let a = Array.init(len.Length)(fun i ->
        let ans = [| len.[i]; w.[i]; h.[i]; |]
        System.Array.Sort ans; ans)
    System.Array.Sort(a, compare)
    seq {
        for i = 0 to a.Length - 2 do
            for j = 0 to 2 do yield a.[i].[j] < a.[i + 1].[j] }
    |> Seq.forall(id)
let maximumSum(a: int[])(q: int[][]) =
    System.Array.Sort(a)
    let b = Array.zeroCreate<int>(a.Length)
    for st in q do
        match st with
        | [| s; t; |] ->
            for i = s to t do
                b.[i] <- b.[i] + 1
        | _ -> failwith "bad input"
    System.Array.Sort(b)
    Array.fold2(fun s ai bi -> s + ai * bi)(0)(a)(b)
let rowsRearranging(a: int[][]) =
    let p = [| 0 .. a.Length - 1 |]
    let q = Array.init(a.Length)(fun i -> a.[i].[0])
    System.Array.Sort(q, p)
    seq {
        for i = 0 to p.Length - 2 do
            for j = 0 to a.[0].Length - 1 do
                yield a.[p.[i]].[j] < a.[p.[i + 1]].[j] }
    |> Seq.forall(id)
let digitDifferenceSort(a: int[]) =
    System.Array.Reverse(a)
    a.OrderBy(fun i ->
        let s = string i
        int(s.Max()) - int(s.Min())).ToArray()
let uniqueDigitProducts(a: int[]) =
    let f(n: int) = string(n).Aggregate(1, (fun s i ->
        s * ((int i) - (int '0'))))
    a.Select(f).Distinct().Count()

// Time River
let validTime(time: string) =
    int time.[.. 1] < 24 && int time.[3 ..] < 60
let videoPart part total =
    let sec(s: string) =
        (int s.[.. 1] * 60 + int s.[3 .. 4]) * 60 + int s.[6 ..]
    let rec gcd(a: int, b: int) =
        match b with 0 -> a | _ -> gcd(b, a % b)
    let p, q = sec part, sec total
    let r = gcd(p, q) in [| p / r; q / r |]
let dayOfWeek birthdayDate =
    let b = System.DateTime.Parse(birthdayDate)
    let y = if b.Month = 2 && b.Day = 29 then 4 else 1
    let rec f(n: int) =
        let n1 = n + y
        if b.AddYears(n1).DayOfWeek = b.DayOfWeek then n1
        else f(n1) in f(0)
let curiousClock someTime leavingTime =
    let c = System.DateTime.Parse(someTime)
    let t = System.DateTime.Parse(leavingTime)
    let t2 = if t <= c then t else c - (t - c)
    t2.ToString("yyyy-MM-dd HH:mm")
let newYearCelebrations(a: string)(m: int[]) =
    let mutable t = int a.[.. 1] * 60 + int a.[3 ..]
    if t = 0 then t <- 1440
    let mutable c = 0
    for i = 0 to m.Length - 1 do
        let mi = if i = 0 then m.[0] else m.[i] - m.[i - 1]
        let mutable ti = t + mi
        if t <= 1440 && 1440 <= ti then c <- c + 1
        t <- ti - 60
    if t <= 1440 then c + 1 else c
let regularMonths currMonth =
    let a = System.DateTime.ParseExact(currMonth, "MM-yyyy", null)
    let rec f(n: int) =
        let a1 = a.AddMonths(n + 1)
        if a1.DayOfWeek = System.DayOfWeek.Monday then
            a1.ToString("MM-yyyy")
        else f(n + 1) in f(0)
let missedClasses(year)(daysOfTheWeek: int[])(holidays: string[]) =
    let days = Array.zeroCreate<bool>(8)
    for i in daysOfTheWeek do days.[i] <- true
    days.[0] <- days.[7]
    holidays.Count(fun i ->
        let y = if i.[.. 1] < "09" then year + 1 else year
        let d = System.DateTime(y, int i.[.. 1], int i.[3 ..])
        days.[int d.DayOfWeek])
let holiday x weekDay month year =
    let mm = System.DateTime.ParseExact(month, "MMMM", null).Month
    let s = seq {
        for i = 1 to System.DateTime.DaysInMonth(year, mm) do
            let date = System.DateTime(year, mm, i)
            if date.DayOfWeek.ToString() = weekDay then yield i }
    try s |> Seq.item(x - 1) with _ -> -1

// Chess Tavern
let bishopAndPawn(bishop: string)(pawn: string) =
    let dx = int bishop.[0] - int pawn.[0]
    let dy = int bishop.[1] - int pawn.[1]
    abs dx = abs dy
let chessKnightMoves(cell: string) =
    let dx = [| -2; -2; -1; -1;  1;  1;  2;  2 |]
    let dy = [| -1;  1; -2;  2; -2;  2; -1;  1 |]
    (seq { 0 .. 7 }).Count(fun i ->
        let c1 = int cell.[0] + dx.[i] |> char
        let c2 = int cell.[1] + dy.[i] |> char
        'a' <= c1 && c1 <= 'h' &&
        '1' <= c2 && c2 <= '8')
let bishopDiagonal(bishop1: string)(bishop2: string) =
    let f(bis1: string, bis2: string) =
        let x1 = int bis1.[0]
        let y1 = int bis1.[1]
        let x2 = int bis2.[0]
        let y2 = int bis2.[1]
        let cx = int(if x2 - x1 < 0 then 'h' else 'a') - x1 |> abs
        let cy = int(if y2 - y1 < 0 then '8' else '1') - y1 |> abs
        let c = min(cx)(cy)
        let x3 = if x2 - x1 < 0 then x1 + c else x1 - c
        let y3 = if y2 - y1 < 0 then y1 + c else y1 - c
        System.String([| char x3; char y3 |])
    let dx = int bishop2.[0] - int bishop1.[0]
    let dy = int bishop2.[1] - int bishop1.[1]
    let a =
        if abs dx = abs dy then
            [| f(bishop1, bishop2); f(bishop2, bishop1) |]
        else [| bishop1; bishop2 |]
    System.Array.Sort(a); a
let whoseTurn(p: string) =
    p.Aggregate(0, fun s i -> s ^^^ int i) &&& 1 <> 0
let chessBishopDream(boardSize: int[])(initP: int[])(initD: int[])(k: int) =
    [| for i = 0 to 1 do
        let a = initP.[i] + initD.[i] * k
        let b = boardSize.[i] <<< 1
        let c = (a % b + b) % b
        yield min(b - 1 - c)(c) |]
let chessTriangle n m =
    let f(n, m) =
        let n1 = max(0)(n - 1)
        let n2 = max(0)(n - 2)
        let m2 = max(0)(m - 2)
        let m3 = max(0)(m - 3)
        (n1 + n2) * (m2 + m3) <<< 3
    f(n, m) + f(m, n)
type BKStatus =
    | Checkmate | Check | Stalemate | ValidMove
    | NearKing | OnAmazon | Invalid
let amazonCheckmate(king: string)(amazon: string) =
    let nearKing(pos: string) =
        let dx = int pos.[0] - int king.[0] |> abs
        let dy = int pos.[1] - int king.[1] |> abs
        (dx <= 1) && (dy <= 1)
    let inDanger(pos: string) =
        if pos = amazon then OnAmazon
        elif nearKing(pos) then NearKing else
            let dx = int pos.[0] - int amazon.[0]
            let dy = int pos.[1] - int amazon.[1]
            let b =
                (abs dx <= 2 && abs dy <= 2) ||
                let kx = int king.[0] - int amazon.[0]
                let ky = int king.[1] - int amazon.[1]
                let b1() =
                    (dx = 0) && not ((kx = 0) &&
                        ((0 < ky && ky < dy) || (0 > ky && ky > dy)))
                let b2() =
                    (dy = 0) && not ((ky = 0) &&
                        ((0 < kx && kx < dx) || (0 > kx && kx > dx)))
                let b3() =
                    (dx = dy) && not ((kx = ky) &&
                        ((0 < kx && kx < dx) || (0 > kx && kx > dx)))
                let b4() =
                    (dx = -dy) && not ((kx = -ky) &&
                        ((0 < kx && kx < dx) || (0 > kx && kx > dx)))
                b1() || b2() || b3() || b4()
            if b then Check else ValidMove
    let dx = [| -1; -1; -1;  0;  0;  1;  1;  1 |]
    let dy = [| -1;  0;  1; -1;  1; -1;  0;  1 |]
    let canMove(pos: string) =
        (seq { 0 .. 7 }).Any(fun i ->
            let pos1x = int pos.[0] + dx.[i] |> char
            let pos1y = int pos.[1] + dy.[i] |> char
            'a' <= pos1x && pos1x <= 'h' &&
            '1' <= pos1y && pos1y <= '8' &&
            match inDanger(System.String([| pos1x; pos1y |])) with
            | ValidMove -> true
            | OnAmazon -> amazon |> nearKing |> not
            | _ -> false)
    let bkStatus(pos) =
        match inDanger(pos) with
        | OnAmazon | NearKing -> Invalid
        | Check -> if canMove(pos) then Check else Checkmate
        | ValidMove -> if canMove(pos) then ValidMove else Stalemate
        | _ -> failwith "unexpected return value from inDanger"
    let ret = Array.zeroCreate<int>(4)
    for i = int 'a' to int 'h' do
        for j = int '1' to int '8' do
            let pos = System.String([| i |> char; j |> char |])
            match pos |> bkStatus with
            | Checkmate  -> ret.[0] <- ret.[0] + 1
            | Check      -> ret.[1] <- ret.[1] + 1
            | Stalemate  -> ret.[2] <- ret.[2] + 1
            | ValidMove  -> ret.[3] <- ret.[3] + 1
            | _ -> ()
    ret
let pawnRace(white: string)(black: string)(toMove) =
    let mover1 = if toMove = 'w' then "white" else "black"
    let mover2 = if toMove = 'w' then "black" else "white"
    let greedy() =
        let ws = if white.[1] = '2' then 5 else int '8' - int white.[1]
        let bs = if black.[1] = '7' then 5 else int black.[1] - int '1'
        if ws < bs then "white"
        elif ws > bs then "black"
        else mover1
    if white.[1] >= black.[1] then greedy() else
        match abs(int white.[0] - int black.[0]) with
        | 0 -> "draw"
        | 1 ->
            match int black.[1] - int white.[1] with
            | 1 -> mover1  | 2 -> mover2  | 3 -> mover1
            | 4 -> if black.[1] = '6' then "white" else "black"
            | 5 -> mover2  | _ -> failwith "bad logic"
        | _ -> greedy()

// Regular Hell
open System.Text.RegularExpressions
let isSentenceCorrect sentence =
    let regex = "^[A-Z][^.?!]*[.?!]$"
    Regex.IsMatch(sentence, regex)
let replaceAllDigitsRegExp input =
    input |> String.map(fun i ->
        if System.Char.IsDigit(i) then '#' else i)
let swapAdjacentWords s =
    Regex.Replace(s, "([^ ]+) ([^ ]+)", "$2 $1")
let nthNumber s n =
    let regex =
        let s = System.Text.StringBuilder()
        for i = 1 to n - 1 do
            s.Append("0*[0-9]+[^0-9]+") |> ignore
        s.Append("0*([0-9]+)").ToString()
    Regex.Match(s, regex).Groups.[1].Value
let isSubsequence t s =
    let regex = String.collect (fun ch ->
        match ch with
        | '.' | '?' | '*' | '+' | '-' -> ".*\\" + string ch
        | _ -> ".*" + string ch) s + ".*"
    Regex.Match(t, regex).Success
let eyeRhyme pairOfLines =
    let regex = ".*(.{3})\t.*(.{3})"
    let result = Regex.Match(pairOfLines, regex)
    System.String.Equals(result.Groups.[1].Value,
                         result.Groups.[2].Value)
let programTranslation solution args =
    let argumentVariants = args |> String.concat "|"
    let pattern = "(?<!\\$)\\b(" + argumentVariants + ")\\b"
    let replacement = "$$$1"
    Regex.Replace(solution, pattern, replacement)
let repetitionEncryption letter =
    let pattern = "(?i)\\b(\\w+)\\W+\\1\\b"
    Regex.Matches(letter, pattern).Count
let bugsAndBugfixes rules =
    let pattern = "(\\d*)d(\\d+)([+-]\d+)?"
    let formulas = Regex.Matches(rules, pattern)
    let mutable res = 0
    for formula in formulas do
        let groups = formula.Groups
        let rolls = if System.String.IsNullOrEmpty(groups.[1].Value)
                    then
                        1
                    else
                        System.Int32.Parse(groups.[1].Value)
        let dieType = System.Int32.Parse(groups.[2].Value)
        let mutable formulaMax = rolls * dieType
        if not(System.String.IsNullOrEmpty(groups.[3].Value)) then
            if groups.[3].Value.[0] = '-' then
                formulaMax <- formulaMax - System.Int32.Parse(groups.[3].Value.Substring(1))
            else
                formulaMax <- formulaMax + System.Int32.Parse(groups.[3].Value.Substring(1))
        res <- res + formulaMax
    res

// Secret Archives
let lrc2subRip(lrc: string[])(songLength: string) =
    let songL = songLength + ",000"
    let convert(a: string) =
        let m = a.[1 .. 2] |> System.Int32.Parse
        sprintf "%02d:%02d:%s,%s0" (m / 60) (m % 60) a.[4 .. 5] a.[7 .. 8]
    let text(a: string) = if a.Length > 11 then a.[11 ..] else ""
    let ret = Array.zeroCreate<string>((lrc.Length <<< 2) - 1)
    let mutable z = 0
    let mutable time0 = convert(lrc.[0])
    let mutable time1 = null: string
    for i = 1 to lrc.Length do
        time1 <- time0
        time0 <- if i = lrc.Length then songL else convert(lrc.[i])
        ret.[z] <- i |> string
        ret.[z + 1] <- time1 + " --> " + time0
        ret.[z + 2] <- lrc.[i - 1] |> text
        if i < lrc.Length then ret.[z + 3] <- ""
        z <- z + 4
    ret
let htmlTable(table: string)(row)(column) =
    use sr = new System.IO.StringReader(table)
    use xml = System.Xml.XmlReader.Create(sr)
    let mutable ret = null: string
    let mutable r = row + 1
    let mutable c = column + 1
    while xml.Read() do
        if xml.NodeType = System.Xml.XmlNodeType.Element then
            match xml.Name with
            | "tr" -> r <- r - 1
            | "td" ->
                if r = 0 then
                    c <- c - 1
                    if c = 0 then ret <- xml.ReadInnerXml()
            | _ -> ()
    match ret with
    | null -> "No such cell"
    | _ -> ret
let chessNotation(notation) =
    let johnMe(s: string) =
        let ret = Array2D.create<char>(8)(8)('.')
        let ss = s.Split('/')
        for i = 0 to 7 do
            let mutable z = 0
            for j in ss.[i] do
                if System.Char.IsDigit(j) then z <- z + (int j - int '0')
                else ret.[i, z] <- j; z <- z + 1
        ret
    let rotate(s: char[,]) = Array2D.init(8)(8)(fun i j -> s.[7 - j, i])
    let meJohn(s: char[,]) =
        let a = System.Text.StringBuilder()
        for i = 0 to 7 do
            if i > 0 then a.Append('/') |> ignore
            let mutable z = 0
            for j = 0 to 7 do
                if s.[i, j] = '.' then z <- z + 1 else
                    if z > 0 then a.Append(z) |> ignore; z <- 0
                    a.Append(s.[i, j]) |> ignore
            if z > 0 then a.Append(z) |> ignore
        a.ToString()
    notation |> johnMe |> rotate |> meJohn
let cellsJoining(a: string[])(coords: int[][]) =
    let xs = [|
        for i = 0 to a.Length - 1 do
            if a.[i].[0] = '+' then yield i |]
    let ys = [|
        for i = 0 to a.[0].Length - 1 do
            if a.[0].[i] = '+' then yield i |]
    let c = a |> Array.map(fun i -> i.ToCharArray())
    match coords with
    | [| [| x2; y1 |]; [| x1; y2 |] |] ->
        for i = xs.[x1] + 1 to xs.[x2 + 1] - 1 do
            for j = ys.[y1] + 1 to ys.[y2 + 1] - 1 do
                match c.[i].[j] with
                | '-' | '|' | '+' -> c.[i].[j] <- ' '
                | _ -> ()
        if ys.[y1] = 0 then
            for i = xs.[x1] + 1 to xs.[x2 + 1] - 1 do
                if c.[i].[0] = '+' then c.[i].[0] <- '|'
        if ys.[y2 + 1] = c.[0].Length - 1 then
            let z = c.[0].Length - 1
            for i = xs.[x1] + 1 to xs.[x2 + 1] - 1 do
                if c.[i].[z] = '+' then c.[i].[z] <- '|'
        if xs.[x1] = 0 then
            for i = ys.[y1] + 1 to ys.[y2 + 1] - 1 do
                if c.[0].[i] = '+' then c.[0].[i] <- '-'
        if xs.[x2 + 1] = c.Length - 1 then
            let z = c.Length - 1
            for i = ys.[y1] + 1 to ys.[y2 + 1] - 1 do
                if c.[z].[i] = '+' then c.[z].[i] <- '-'
    | _ -> failwith "bad input"
    c |> Array.map(fun i -> System.String(i))
let firstOperationCharacter(a: string) =
    let a = seq {
        let d = ref 0
        for i = 0 to a.Length - 1 do
            match a.[i] with
            | '(' -> d := !d + 1
            | ')' -> d := !d - 1
            | '+' -> yield (!d, 0, -i)
            | '*' -> yield (!d, 1, -i)
            | _ -> () }
    match a.Max() with (_, _, p) -> -p
let countElements(a: string) =
    let mutable ret = 0
    let mutable i = 0
    while i < a.Length do
        match a.[i] with
        | ' ' | '[' | ']' | ',' -> i <- i + 1
        | '"' ->
            ret <- ret + 1
            i <- i + 1
            while a.[i] <> '"' do i <- i + 1
            i <- i + 1
        | _ ->
            ret <- ret + 1
            let cont(i) =
                match a.[i] with
                | ' ' | '[' | ']' | ',' -> false
                | _ -> true
            while i < a.Length && cont(i) do i <- i + 1
    ret
let treeBottom(e: string) =
    let p = ref 0
    let skipSpace() = while e.[!p] = ' ' do p := !p + 1
    let parseInt() =
        skipSpace()
        printfn "p: %d" !p
        let p0 = !p
        while System.Char.IsDigit(e.[!p]) do p := !p + 1
        printfn "%s" e.[p0 .. !p - 1]
        System.Int32.Parse(e.[p0 .. !p - 1])
    let rec parseTerm() =
        skipSpace()
        p := !p + 1 // '('
        if e.[!p] = ')' then
            p := !p + 1
            (0, Array.empty<int>)
        else
            let n = parseInt()
            let (n1, a1) = parseTerm()
            let (n2, a2) = parseTerm()
            p := !p + 1 // ')'
            if n1 < n2 then (n2 + 1, a2)
            elif n2 < n1 then (n1 + 1, a1)
            elif n1 = 0 then (1, [| n |])
            else (n1 + 1, [| a1; a2 |] |> Array.concat)
    match parseTerm() with (_, a) -> a
let befunge93(a: string[]) =
    let s = System.Collections.Generic.Stack<int>()
    let ou = System.Text.StringBuilder()
    let x = ref 0
    let y = ref 0
    let dx = ref 0
    let dy = ref 1
    let c = ref 0
    let stop = ref false
    let next() =
        x := (!x + !dx + a.Length) % a.Length
        y := (!y + !dy + a.[0].Length) % a.[0].Length
    let pop() = if s.Count = 0 then 0 else s.Pop()
    while not !stop do
        if !c >= 100000 then stop := true
        elif ou.Length >= 100 then stop := true else
            match a.[!x].[!y] with
            | '@' -> stop := true
            | ' ' -> next()
            | '"' ->
                next()
                while a.[!x].[!y] <> '"' do
                    s.Push(int a.[!x].[!y])
                    next()
                next()
            | k when '0' <= k && k <= '9' ->
                s.Push(int k - int '0')
                next()
            | ',' -> ou.Append(pop() |> char) |> ignore; next()
            | '.' ->
                ou.Append(pop()) |> ignore
                ou.Append(' ') |> ignore; next()
            | '$' -> pop() |> ignore; next()
            | '\\' ->
                let a = pop() in let b = pop()
                s.Push(a); s.Push(b); next()
            | ':' -> let a = pop() in s.Push(a); s.Push(a); next()
            | '`' ->
                let a = pop() in let b = pop()
                s.Push(if b > a then 1 else 0); next();
            | '!' -> s.Push(if pop() = 0 then 1 else 0); next()
            | '%' -> let a = pop() in let b = pop() in s.Push(b % a); next()
            | '/' -> let a = pop() in let b = pop() in s.Push(b / a); next()
            | '*' -> let a = pop() in let b = pop() in s.Push(b * a); next()
            | '-' -> let a = pop() in let b = pop() in s.Push(b - a); next()
            | '+' -> let a = pop() in let b = pop() in s.Push(b + a); next()
            | '|' ->
                if pop() = 0 then dx := 1; dy := 0
                else dx := -1; dy := 0
                next()
            | '_' ->
                if pop() = 0 then dx := 0; dy := 1
                else dx := 0; dy := -1
                next()
            | '#' -> next(); next()
            | '^' -> dx := -1;  dy := 0;   next()
            | 'v' -> dx := 1;   dy := 0;   next()
            | '<' -> dx := 0;   dy := -1;  next()
            | '>' -> dx := 0;   dy := 1;   next()
            | _ -> failwith "bad input"
            c := !c + 1
    ou.ToString()

// Cliffs of Pain
(* 111 / 300 *)
[<AllowNullLiteral>]
type Grid =
    val Le: char[,]
    val Up: char[,]
    new (n, m) = {
        Le = Array2D.zeroCreate<char>(n)(m + 1)
        Up = Array2D.zeroCreate<char>(n + 1)(m)
    }
    new (grid: Grid) = {
        Le = grid.Le.Clone() :?> char[,]
        Up = grid.Up.Clone() :?> char[,]
    }
let pipesGame(st: string[]) =
    let g0 = Grid(st.Length, st.[0].Length)
    for i = 0 to st.Length - 1 do
        for j = 0 to st.[0].Length - 1 do
            if st.[i].[j] |> System.Char.IsLower then
                g0.Le.[i, j]      <- st.[i].[j]
                g0.Le.[i, j + 1]  <- st.[i].[j]
                g0.Up.[i, j]      <- st.[i].[j]
                g0.Up.[i + 1, j]  <- st.[i].[j]
    let tick(g: Grid) =
        let ret = Grid(g)
        let fail = ref false
        let flow(a: char[,], i1, i2, src) =
            if a.[i1, i2] = src then ()
            elif a.[i1, i2] = char 0 then a.[i1, i2] <- src
            else fail := true
        let assertLeq(dst, src) =
            let dstL = System.Char.ToLower(dst)
            if dstL <> src then fail := true
        for i = 0 to st.Length - 1 do
            for j = 0 to st.[0].Length do
                let curr = g.Le.[i, j]
                if curr <> char 0 then
                    if j > 0 then
                        match st.[i].[j - 1] with
                        | '0' | '1' | '4' | '5' ->
                            if j < st.[0].Length && st.[i].[j] <> curr then
                                fail := true
                        | '2' | '7' -> flow(ret.Le, i, j - 1, curr)
                        | '3' -> flow(ret.Up, i + 1, j - 1, curr)
                        | '6' -> flow(ret.Up, i, j - 1, curr)
                        | a -> assertLeq(a, curr)
                    elif st.[i].[j] <> curr then fail := true
                    if j < st.[0].Length then
                        match st.[i].[j] with
                        | '0' | '1' | '3' | '6' ->
                            if j > 0 && st.[i].[j - 1] <> curr then
                                fail := true
                        | '2' | '7' -> flow(ret.Le, i, j + 1, curr)
                        | '4' -> flow(ret.Up, i + 1, j, curr)
                        | '5' -> flow(ret.Up, i, j, curr)
                        | a -> assertLeq(a, curr)
                    elif st.[i].[j - 1] <> curr then fail := true
        for i = 0 to st.Length do
            for j = 0 to st.[0].Length - 1 do
                let curr = g.Up.[i, j]
                if curr <> char 0 then
                    if i > 0 then
                        match st.[i - 1].[j] with
                        | '0' | '2' | '5' | '6' ->
                            if i < st.Length && st.[i].[j] <> curr then
                                fail := true
                        | '1' | '7' -> flow(ret.Up, i - 1, j, curr)
                        | '3' -> flow(ret.Le, i - 1, j + 1, curr)
                        | '4' -> flow(ret.Le, i - 1, j, curr)
                        | a -> assertLeq(a, curr)
                    elif st.[i].[j] <> curr then fail := true
                    if i < st.Length then
                        match st.[i].[j] with
                        | '0' | '2' | '3' | '4' ->
                            if i > 0 && st.[i - 1].[j] <> curr then
                                fail := true
                        | '1' | '7' -> flow(ret.Up, i + 1, j, curr)
                        | '5' -> flow(ret.Le, i, j, curr)
                        | '6' -> flow(ret.Le, i, j + 1, curr)
                        | a -> assertLeq(a, curr)
                    elif st.[i - 1].[j] <> curr then fail := true
        if !fail then null else ret
    let count(g: Grid) =
        let ans = ref 0
        for i = 0 to st.Length - 1 do
            for j = 0 to st.[0].Length - 1 do
                match st.[i].[j] with
                | '1' ->
                    g.Up.[i, j] <> char 0 &&
                    g.Up.[i + 1, j] <> char 0
                | '2' ->
                    g.Le.[i, j] <> char 0 &&
                    g.Le.[i, j + 1] <> char 0
                | '3' ->
                    g.Le.[i, j + 1] <> char 0 &&
                    g.Up.[i + 1, j] <> char 0
                | '4' ->
                    g.Le.[i, j] <> char 0 &&
                    g.Up.[i + 1, j] <> char 0
                | '5' ->
                    g.Le.[i, j] <> char 0 &&
                    g.Up.[i, j] <> char 0
                | '6' ->
                    g.Le.[i, j + 1] <> char 0 &&
                    g.Up.[i, j] <> char 0
                | '7' ->
                    g.Up.[i, j] <> char 0 &&
                    g.Up.[i + 1, j] <> char 0 ||
                    g.Le.[i, j] <> char 0 &&
                    g.Le.[i, j + 1] <> char 0
                | _ -> false
                |> (fun b -> if b then ans := !ans + 1)
        !ans
    let rec f(g, cg) =
        match tick(g) with
        | null -> -count(g)
        | g1 ->
            let cg1 = count(g1)
            if cg = cg1 then cg else f(g1, cg1)
    f(g0, 0)
    (*let writeGrid(g: Grid) =
        let f(c: char) = if c = char 0 then ' ' else c
        let h(c: char) = if c = '0' then ' ' else c
        for i = 0 to st.Length do
            for j = 0 to st.[0].Length - 1 do
                printf "+%c" (g.Up.[i, j] |> f)
            printfn "+"
            if i < st.Length then
                for j = 0 to st.[0].Length - 1 do
                    printf "%c%c" (g.Le.[i, j] |> f) (st.[i].[j] |> h)
                printfn "%c" g.Le.[i, st.[0].Length]
        printfn "==================================" *)
let game2048(grid: int[][])(path: string) =
    let pressL(grid: int[][]) =
        grid |> Array.map(fun a ->
            let ret = Array.zeroCreate<int>(4)
            let rec f(m, a1, z) =
                if m = a.Length then
                    if a1 <> -1 then ret.[z] <- a1
                elif a.[m] = 0 then f(m + 1, a1, z)
                elif a1 = -1 then f(m + 1, a.[m], z)
                elif a1 = a.[m] then
                    ret.[z] <- a.[m] + a1
                    f(m + 1, -1, z + 1)
                else
                    ret.[z] <- a1
                    f(m + 1, a.[m], z + 1)
            f(0, -1, 0); ret)
    let rotate(grid: int[][]) =
        Array.init(4)(fun i ->
            Array.init(4)(fun j -> grid.[3 - j].[i]))
    let press(grid: int[][], dir) =
        match dir with
        | 'L' -> grid |> pressL
        | 'D' -> grid |> rotate |> pressL |> rotate |> rotate |> rotate
        | 'R' -> grid |> rotate |> rotate |> pressL |> rotate |> rotate
        | 'U' -> grid |> rotate |> rotate |> rotate |> pressL |> rotate
        | _ -> failwith "bad input"
    let rec f(m, gr) =
        if m = path.Length then gr else
            f(m + 1, press(gr, path.[m]))
    f(0, grid)
let dir4 = [| 0, 1; -1, 0; 0, -1; 1, 0 |]
let arrow4 = ">^<v"
type Snake(b: char[][]) =
    let mutable headX = -1
    let mutable headY = -1
    let mutable d = -1
    do for i = 0 to b.Length - 1 do
        for j = 0 to b.[0].Length - 1 do
            match b.[i].[j] with
            | '>' -> headX <- i; headY <- j; d <- 0
            | '^' -> headX <- i; headY <- j; d <- 1
            | '<' -> headX <- i; headY <- j; d <- 2
            | 'v' -> headX <- i; headY <- j; d <- 3
            | _ -> ()
    let a = System.Collections.Generic.Queue<int * int>()
    let a0 = ResizeArray<int * int>()
    let rec f(x, y) =
        b.[x].[y] <- '.'
        a0.Add(x, y)
        let rec g(m) =
            if m < 4 then
                let x1 = x + (fst dir4.[m])
                let y1 = y + (snd dir4.[m])
                if
                    0 <= x1 && x1 < b.Length &&
                    0 <= y1 && y1 < b.[0].Length &&
                    b.[x1].[y1] = '*'
                then f(x1, y1) else g(m + 1)
        g(0)
    do f(headX, headY)
    do for i = a0.Count - 1 downto 0 do
        a.Enqueue(a0.[i])
        b.[fst a0.[i]].[snd a0.[i]] <- '*'
    do b.[headX].[headY] <- arrow4.[d]
    member u.Tick(c) =
        match c with
        | 'L' -> d <- (d + 1) &&& 3; b.[headX].[headY] <- arrow4.[d]
        | 'R' -> d <- (d + 3) &&& 3; b.[headX].[headY] <- arrow4.[d]
        | 'F' ->
            let aHX, aHY = a.Dequeue()
            b.[aHX].[aHY] <- '.'
            let nX = headX + (fst dir4.[d])
            let nY = headY + (snd dir4.[d])
            if
                0 <= nX && nX < b.Length &&
                0 <= nY && nY < b.[0].Length &&
                b.[nX].[nY] = '.'
            then
                b.[headX].[headY] <- '*'
                b.[nX].[nY] <- arrow4.[d]
                a.Enqueue(nX, nY)
                headX <- nX
                headY <- nY
            else
                d <- -1
                b.[aHX].[aHY] <- 'X'
                for i = 0 to b.Length - 1 do
                    for j = 0 to b.[0].Length - 1 do
                        if b.[i].[j] <> '.' then b.[i].[j] <- 'X'
        | _ -> failwith "bad input"
    member u.D = d
    member u.B = b
let snakeGame(b: char[][])(c: string) =
    let sn = Snake(b)
    let rec f(m) =
        if m = c.Length || sn.D = -1 then sn.B else
            sn.Tick(c.[m])
            f(m + 1)
    f(0)
let tetrisGame(a: char[][][]) =
    let board = ResizeArray<char[]>()
    let ans = ref 0
    let rotate90(a: char[][]) =
        Array.init(a.[0].Length)(fun i ->
            Array.init(a.Length)(fun j -> a.[a.Length - 1 - j].[i]))
    let collisionFree(curr: char[][], x, y) =
        seq {0 .. curr.Length - 1} |> Seq.forall(fun i ->
            let iBoard = y + curr.Length - 1 - i
            iBoard >= board.Count ||
            seq {0 .. curr.[0].Length - 1} |> Seq.forall(fun j ->
                board.[iBoard].[x + j] = '.' || curr.[i].[j] = '.'))
    let blockSum(curr: char[][], y) =
        let mutable ans = 0
        for i = 0 to curr.Length - 1 do
            let iBoard = y + curr.Length - 1 - i
            if iBoard < board.Count then
                ans <- ans + board.[iBoard].Count(fun i -> i = '#')
        ans
    let place(curr: char[][], x, y) =
        let ensuredCount = y + curr.Length
        while board.Count < ensuredCount do
            board.Add(Array.create<char>(10)('.'))
        for i = 0 to curr.Length - 1 do
            let iBoard = y + curr.Length - 1 - i
            for j = 0 to curr.[0].Length - 1 do
                if curr.[i].[j] = '#' then board.[iBoard].[x + j] <- '#'
        let mutable z = y
        for i = y to board.Count - 1 do
            if board.[i].Count(fun i -> i = '#') = 10 then
                ans := !ans + 1
            else
                board.[z] <- board.[i]
                z <- z + 1
        board.RemoveRange(z, board.Count - z)
    for ai in a do
        let mutable currOpt = null: char[][]
        let mutable xOpt = -1
        let mutable yOpt = -1
        let mutable opt = -1
        let mutable curr = ai
        for rotate = 0 to 3 do
            for x = 0 to 10 - curr.[0].Length do
                let mutable y = board.Count
                while y > 0 && collisionFree(curr, x, y - 1) do y <- y - 1
                let t = blockSum(curr, y)
                if t > opt then
                    opt <- t
                    currOpt <- curr
                    xOpt <- x
                    yOpt <- y
            curr <- rotate90(curr)
        place(currOpt, xOpt, yOpt)
    !ans
(* 43 / 300 *)
let linesGame
    (field: char[][])
    (clicks: int[][])
    (newBalls: char[])
    (newBallsCoordinates: int[][]) =
    let mutable ans = 0
    let k = ref 0
    let mutable ballX = -1
    let mutable ballY = -1
    let dX = [|0; -1;  0; 1|]
    let dY = [|1;  0; -1; 0|]
    let isBall(x, y) = field.[x].[y] |> System.Char.IsLetter
    let isConnected(oldX, oldY, newX, newY) =
        let canReach = Array2D.zeroCreate<bool>(9)(9)
        let q = System.Collections.Generic.Queue<int * int>()
        canReach.[oldX, oldY] <- true
        q.Enqueue(oldX, oldY)
        while q.Count <> 0 && not canReach.[newX, newY] do
            let qHX, qHY = q.Dequeue()
            for i = 0 to 3 do
                let aX = qHX + dX.[i]
                let aY = qHY + dY.[i]
                let validX = 0 <= aX && aX < 9
                let validY = 0 <= aY && aY < 9
                let valid = validX && validY && not(isBall(aX, aY))
                if valid && not canReach.[aX, aY] then
                    canReach.[aX, aY] <- true
                    q.Enqueue(aX, aY)
        canReach.[newX, newY]
    let dX8 = [|1; 0; 1;  1|]
    let dY8 = [|0; 1; 1; -1|]
    let appear(newX, newY, c) =
        field.[newX].[newY] <- c
        let rec go(pX, pY, dX, dY, ans, fill) =
            let cX = pX + dX
            let cY = pY + dY
            let validX = 0 <= cX && cX < 9
            let validY = 0 <= cY && cY < 9
            if validX && validY && field.[cX].[cY] = c then
                if fill then field.[cX].[cY] <- '.'
                go(cX, cY, dX, dY, ans + 1, fill)
            else ans
        let mutable ans = -1
        for i = 0 to 3 do
            let lower = go(newX, newY, -dX8.[i], -dY8.[i], 0, false)
            let upper = go(newX, newY,  dX8.[i],  dY8.[i], 0, false)
            if lower + upper >= 4 then
                ans <- ans + (lower + upper + 2)
                field.[newX].[newY] <- '.'
                go(newX, newY, -dX8.[i], -dY8.[i], 0, true) |> ignore
                go(newX, newY,  dX8.[i],  dY8.[i], 0, true) |> ignore
        if ans <> -1 then ans else 0
    let move(oldX, oldY, newX, newY) =
        let c = field.[oldX].[oldY]
        field.[oldX].[oldY] <- '.'
        let mutable ans = appear(newX, newY, c)
        if ans = 0 then
            for i = 1 to 3 do
                let nbX = newBallsCoordinates.[!k].[0]
                let nbY = newBallsCoordinates.[!k].[1]
                let nbC = newBalls.[!k]
                ans <- ans + appear(nbX, nbY, nbC)
                k := !k + 1
        ans
    for i in clicks do
        if isBall(i.[0], i.[1]) then
            ballX <- i.[0]
            ballY <- i.[1]
        elif ballX <> -1 then
            if isConnected(ballX, ballY, i.[0], i.[1]) then
                ans <- ans + move(ballX, ballY, i.[0], i.[1])
            ballX <- -1
    ans
